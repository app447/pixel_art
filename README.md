# pixel_art



## Installation


```
python -m venv env
. env/bin/activate (unix)
. .\env\Scripts\activate (windows)

pip install -r requirements.txt
```

## How to use 

```
python pixel_art.py  --word 'guegz' --pattern 'oh'
```


```
ohohohoh  oh    oh  ohohohoh  ohohohoh  ohohohohoh  
oh        oh    oh  oh        oh              oh    
oh  ohoh  oh    oh  ohoh      oh  ohoh      oh      
oh    oh  oh    oh  oh        oh    oh    oh        
ohohohoh    ohoh    ohohohoh  ohohohoh  ohohohohoh  
```

copied to clipboard and ready to use ! :)

### Options :
- ```-g, --google_chat``` : to be google chat compatible
- ```-e, --emoji``` : to interpret pattern as emoji. (ex : ':thumbs_up:')
