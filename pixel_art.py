import argparse

from alphabet.standard import standard
from word import Word
import pyperclip


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--word',               type=str, required=True, help='Word to write')
    parser.add_argument('--pattern',            type=str, required=True, help='Pattern to construct the word')
    parser.add_argument('-g','--google_chat',   action="store_true")
    parser.add_argument('-e', '--emoji',        action="store_true")
    args = parser.parse_args()

    word            = args.word
    pattern         = args.pattern
    google_chat     = args.google_chat
    _emoji          = args.emoji

    w = Word(alphabet=standard, pattern=pattern, google_chat=google_chat, _emoji=_emoji)
    w.construct(word)

    pyperclip.copy(w.__str__()) # copy to clipboard

    print(w)
