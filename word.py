from alphabet.standard import standard
import numpy as np
import emoji


class Word:
    def __init__(
        self, 
        alphabet    = standard, 
        pattern     = 'X',
        google_chat = False,
        _emoji      = False
    ):
        self.alphabet       = alphabet
        self.word           = None # numpy array
        self.pattern        = pattern if not(_emoji) else emoji.emojize(pattern)
        self.google_chat    = google_chat
        self._emoji         = _emoji

    
    def construct(self, word):
        upper_word = word.upper()

        result = self.alphabet[upper_word[0]]

        for letter in upper_word[1:]:
            result = np.concatenate((result, self.alphabet[letter]), axis=1)
        
        self.word = result

        return self

    def underline(self):
        self.word = np.concatenate((self.word, np.array([[1] * len(self.word[0])])))

        return self

    def set_pattern(self, pattern):
        self.pattern = pattern

        return self

    def __fill(self):
        coef    = 2 if self._emoji else 1
        filled  = np.where(self.word == 1, self.pattern, ' ' * len(self.pattern) * coef)

        return filled

    def __str__(self):
        filled = self.__fill()    
        filled_str = ''
        for line in filled:
            filled_str += ''.join(line) + '\n'
        
        if self.google_chat:
            return "```\n{}```".format(filled_str)

        # pyperclip.copy(filled_str) # copy to clipboard

        return filled_str        
    
    def write(
        self, 
        file = 'result.txt'
    ):

        with open(file,'w') as f:
            f.write(self.__str__())